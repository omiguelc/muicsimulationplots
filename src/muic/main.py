import argparse
import sys

import matplotlib
import numpy as np
from matplotlib import pyplot as plt


def main():
    # CONFIGURE RUN
    if sys.version_info[0] < 3:
        raise AssertionError('Please run this code with Python 3.')

    # Get runtime
    from muic.core.execution import runtime

    # Get plot style and color map
    plt.style.use(runtime.resource('tdrstyle.mplstyle'))

    # Get logger
    from muic.core.execution.runtime import get_logger

    get_logger().info('Debug Mode       : {}'.format(runtime.debug_en))
    get_logger().info('Using python     : {}'.format(sys.version.replace('\n', '')))
    get_logger().info('Using numpy      : {}'.format(np.__version__))
    get_logger().info('Using matplotlib : {}'.format(matplotlib.__version__))

    # CONFIGURE ROOT
    import muic.core.root.environment as root_env

    root_env.configure()

    # CONFIGURE PARSER
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='MuIC Analysis')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Enable debug mode')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    # CONFIGURE COMMANDS
    import muic.commands.pythia.ml_sample.command as ml_sample_cmd
    import muic.commands.pythia.kinematics.command as pythia_kin_cmd
    import muic.commands.pythia.comparisons.command as pythia_comparison_cmd
    import muic.commands.leptoquark.command as leptoquark_cmd
    import muic.commands.zprime.command as zprime_cmd

    command_entries = {
        # Pythia
        'pythia-ml-sample': ('DIS ML Sample', ml_sample_cmd),
        'pythia-kinematics': ('DIS Kinematics', pythia_kin_cmd),
        'pythia-comparison': ('DIS Comparison', pythia_comparison_cmd),
        # ZPRIME
        'zprime': ('Zprime Analysis', zprime_cmd),
        # LQ
        'leptoquark': ('Leptoquark Analysis', leptoquark_cmd),
    }

    for key, command_entry in command_entries.items():
        subparser = subparsers.add_parser(key, help=command_entry[0])
        command_entry[1].configure_parser(subparser)

    args = parser.parse_args()

    # DEBUG MODE
    if args.debug_en:
        runtime.debug_en = True

    # RUN COMMAND
    command_entry = command_entries.get(args.parser)

    if command_entry is None:
        parser.print_help()
        exit(1)

    # RUN
    command_entry[1].run(args)
