import math

import numpy as np

from muic.commands.pythia.commons.parameters import ParameterSet
from muic.core.entries import Producer


class DISReconstructionProducer(Producer):

    def __init__(self, machine):
        self.parameters = ParameterSet(machine)

    def provides(self):
        return [
            'dis_reco_sum_e', 'dis_reco_sum_px', 'dis_reco_sum_py', 'dis_reco_sum_pz', 'dis_reco_sum_e_minus_pz',
            'dis_reco_angljb',
            'dis_reco_Q2_lep', 'dis_reco_x_lep', 'dis_reco_y_lep',
            'dis_reco_Q2_jb', 'dis_reco_x_jb', 'dis_reco_y_jb',
            'dis_reco_Q2_da', 'dis_reco_x_da', 'dis_reco_y_da'
        ]

    def extract(self, event):
        # Lepton Only
        initial_mu_e = self.parameters.machine['Emu']

        mu_e = event.muon.smeared.energy
        mu_theta = event.muon.smeared.p3.Theta()

        q2_lep = 2 * initial_mu_e * mu_e * (1 + np.cos(mu_theta))
        y_lep = 1 - mu_e * (1 - np.cos(mu_theta)) / (2 * initial_mu_e)
        x_lep = q2_lep / (self.parameters.com_energy_squared * y_lep)

        # Hadron Reconstruction
        n = 0
        sum_e = 0
        sum_px = 0
        sum_py = 0
        sum_pz = 0
        sum_e_minus_pz = 0

        for had in event.shower_particles:
            n += 1

            if abs(had.pdgid) in [22, 2112]:
                energy = had.smeared.p
            else:
                energy = had.smeared.energy

            sum_e += energy
            sum_px += had.smeared.px
            sum_py += had.smeared.py
            sum_pz += had.smeared.pz
            sum_e_minus_pz += (energy - had.smeared.pz)

        # Calculate
        sum_pt_squared = pow(sum_px, 2) + pow(sum_py, 2)
        sum_e_minus_pz_squared = pow(sum_e_minus_pz, 2)

        # Jaquet-Blondel
        y_jb = sum_e_minus_pz / (2.0 * initial_mu_e)
        Q2_jb = sum_pt_squared / (1 - y_jb)
        x_jb = Q2_jb / (self.parameters.com_energy_squared * y_jb)

        agljb = math.acos((sum_pt_squared - sum_e_minus_pz_squared) / (sum_pt_squared + sum_e_minus_pz_squared))

        # Double Angle
        Q2_da = 4. * pow(initial_mu_e, 2) * math.sin(agljb) * (math.cos(mu_theta) + 1) / \
                (math.sin(agljb) + math.sin(mu_theta) - math.sin(agljb + mu_theta))
        y_da = math.sin(mu_theta) * (1 - math.cos(agljb)) / \
               (math.sin(agljb) + math.sin(mu_theta) - math.sin(agljb + mu_theta))
        x_da = Q2_da / (self.parameters.com_energy_squared * y_da)

        # Return
        return {
            'dis_reco_sum_e': sum_e,
            'dis_reco_sum_px': sum_px,
            'dis_reco_sum_py': sum_py,
            'dis_reco_sum_pz': sum_pz,
            'dis_reco_sum_e_minus_pz': sum_e_minus_pz,
            'dis_reco_angljb': agljb,
            'dis_reco_Q2_lep': q2_lep,
            'dis_reco_x_lep': x_lep,
            'dis_reco_y_lep': y_lep,
            'dis_reco_Q2_jb': Q2_jb,
            'dis_reco_x_jb': x_jb,
            'dis_reco_y_jb': y_jb,
            'dis_reco_Q2_da': Q2_da,
            'dis_reco_x_da': x_da,
            'dis_reco_y_da': y_da,
        }

    def summary(self):
        pass
