from muic.pythia.primitives.muon import Muon
from muic.pythia.primitives.particle import Particle


def get_muon(event):
    return Muon(event)


def get_shower_particles(event):
    return [
        Particle(event, idx)
        for idx in range(event.vsize_had)
    ]
