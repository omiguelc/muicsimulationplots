from abc import ABC
from abc import abstractmethod


class AbstractAnalyzer(ABC):

    def __init__(self):
        self.plotters = dict()
        self.products = list()
        self.root_products = list()
        self.consolidated = False

    def checkout_plotter(self, clazz, name, *args, **kwargs):
        plotter = self.plotters.get(name, None)

        if plotter is not None:
            return plotter

        # Register Plotter
        plotter = clazz(name, *args, **kwargs)
        self.plotters[name] = plotter

        # Register Product
        self.products.append(plotter)

        # Return
        return plotter

    def register_product(self, obj):
        self.products.append(obj)

    def register_root_product(self, obj):
        self.root_products.append(obj)

    def before_run(self, *args, **kwargs):
        pass

    @abstractmethod
    def process_entry(self, data):
        pass

    def after_run(self, *args, **kwargs):
        pass

    def consolidate(self):
        # Short-Circuit: Already consolidated
        if self.consolidated:
            return

        self.consolidated = True

        # Short-Circuit: Consolidate products
        for product in self.products:
            product.consolidate()

        # Additional consolidation
        self.post_production()

    def post_production(self):
        pass

    def write(self):
        self.consolidate()

        for product in self.products:
            if getattr(product, 'write_en', True):
                product.write()

        for product in self.root_products:
            product.Write()


class AbstractPlotter(ABC):

    def __init__(self):
        self.write_en = True

    @abstractmethod
    def consolidate(self):
        pass

    @abstractmethod
    def write(self):
        pass
