import numpy as np


def uniform_axis(nbins, low, up):
    return np.linspace(low, up, int(nbins + 1))


def uniform_step_axis(step, low, up, align='center'):
    if align == 'center':
        edges = np.arange(low - step * 0.5, up + step * 1.5, step)
    elif align == 'left':
        edges = np.arange(low, up + step * 2., step)
    elif align == 'right':
        edges = np.arange(low - step, up + step, step)

    return edges


def inverted_uniform_step_axis(step, begin, end, add_negatives=False):
    edges = uniform_step_axis(step, begin, end)

    # Invert
    edges = 1 / edges

    # Sort Edges and Remove Zero
    edges = np.sort(edges[edges != 0])

    # Combine
    if add_negatives:
        edges = np.asarray([*(edges[::-1] * -1.), *edges[:]])

    return edges


if __name__ == '__main__':
    print(uniform_axis(10, 0, 10))
    print(uniform_step_axis(1, 0, 10, align='center'))
    print(uniform_step_axis(1, 0, 10, align='left'))
    print(uniform_step_axis(1, 0, 10, align='right'))
