import numpy as np


# Columnar
def nz_sign(val):
    sign = np.sign(val)
    sign = np.where(sign == 0, np.ones_like(sign), sign)
    return sign


def safe_divide(num, denom, def_value=0):
    num, denom = np.asarray(num), np.asarray(denom)

    # IF DENOM IS A SCALAR AND DENOM=0 RETURN A ZERO ARRAY WITH THE SHAPE OF NUM
    # ELSE RETURN NUM DIVIDED BY DENOM
    if denom.ndim == 0:
        return (num - num) if denom == 0 else (num / denom)

    # MAKE SURE THAT X AND Y HAVE THE SAME SHAPE
    if num.ndim > 0 and num.shape != denom.shape:
        raise ValueError('Inconsistent shapes: x.shape={0} y.shape={1}'.format(
            num.shape, denom.shape))

    # TRUE DIVIDE
    invalid_denom_mask = (denom == 0)

    # MAKE ALL NUM WHERE DENOM IS 0, 0
    num = num - (num * invalid_denom_mask)

    # MAKE ALL THE DENOM WHERE DENOM IS 0, 1
    denom = denom + 1.0 * invalid_denom_mask

    # Result for Zero DEN
    result_for_zero_denom = np.ones_like(num) * def_value
    result_for_valid_denom = num / denom

    return np.where(
        invalid_denom_mask,
        result_for_zero_denom,
        result_for_valid_denom
    )
