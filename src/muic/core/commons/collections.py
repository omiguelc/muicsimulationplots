# Dictionaries
def checkout(dictionary, key, val_factory):
    val = dictionary.get(key)

    if val is None:
        val = val_factory()
        dictionary[key] = val

    return val
