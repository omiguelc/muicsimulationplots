import math


def find_quadratic_roots(aX2, bX, c):
    x1 = (-bX + math.sqrt(bX ** 2 - 4 * aX2 * c)) / (2 * aX2)
    x2 = (-bX - math.sqrt(bX ** 2 - 4 * aX2 * c)) / (2 * aX2)
    return (x1, x2)
