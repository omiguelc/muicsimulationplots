import math

import numpy as np

from muic.core.commons.tools import safe_divide, nz_sign

MUON_MASS = 0.105658
PROTON_MASS = 0.938272


def wrap_phi_rad(x):
    # returns phi in [-pi,pi] rad
    twopi = np.pi * 2
    x = x - np.round(x / twopi) * twopi

    return x


def wrap_phi_deg(x):
    # returns phi in [-180.,180] deg
    twopi = 360.
    x = x - np.round(x / twopi) * twopi

    return x


def calc_eta_from_theta_rad(theta_rad):
    theta_sign = nz_sign(theta_rad)
    eta_calc = -1. * theta_sign * np.log(np.tan(np.abs(theta_rad) / 2.))
    eta = np.where(theta_rad == 0, np.ones_like(theta_rad) * np.inf, eta_calc)

    return eta


def calc_eta_from_theta_deg(theta_deg):
    eta = calc_eta_from_theta_rad(np.deg2rad(theta_deg))

    return eta


def calc_theta_rad_from_eta(eta):
    theta_calc = 2 * np.arctan(np.exp(-eta))
    theta = np.where(np.isinf(eta), np.zeros_like(theta_calc), theta_calc)

    return theta


def calc_theta_deg_from_eta(eta):
    theta = np.rad2deg(calc_theta_rad_from_eta(eta))

    return theta


def calc_qpt_from_qinvpt(qinvpt):
    charge = nz_sign(qinvpt)
    pt = safe_divide(1, abs(qinvpt))
    pt = np.where(qinvpt == 0, np.ones_like(qinvpt) * 1e6, pt)
    return charge * pt


def calc_qinvpt_from_qpt(qpt):
    charge = nz_sign(qpt)
    invpt = safe_divide(1, abs(qpt))
    invpt = np.where(qpt == 0, np.ones_like(qpt) * 1e6, invpt)
    return charge * invpt


def calc_d0_cmssw(phi, xv, yv):
    return yv * np.cos(phi) - xv * np.sin(phi)


def calc_z0_cmssw(invpt, eta, phi, vx, vy, vz):
    return vz - ((vx * math.cos(phi) + vy * math.sin(phi)) * abs(invpt)) * math.sinh(eta)
