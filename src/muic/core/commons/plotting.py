import array

import numpy as np
from ROOT import TCanvas
from ROOT import TH1F
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlue


def plot_variables(name, data):
    n_vars = data.shape[1]

    # Build Histograms
    hist_col = list()

    for var_id in range(n_vars):
        var_col = data[:, var_id]

        # Find x range
        max_var = max(var_col)
        min_var = min(var_col)

        # Build one hist per value
        step = (max_var - min_var) / 100

        hist = TH1F('', '', 100, min_var - step, max_var + step)
        hist.FillN(
            var_col.shape[0],
            array.array('d', var_col),
            array.array('d', np.ones_like(var_col))
        )

        # Normalize
        norm = hist.GetSumOfWeights()

        if norm > 0:
            hist.Scale(1 / hist.GetSumOfWeights())

        # Style Hist
        hist.SetLineColor(kBlue)
        hist.SetFillColorAlpha(kBlue, 0.5)
        hist.SetMaximum(1.1)
        hist.SetMinimum(1e-5)

        # Append
        hist_col.append(hist)

    # Plot
    wall_id = 0

    initial_hist_col = hist_col[:]

    while len(initial_hist_col) > 0:
        max_index = min(len(initial_hist_col), 30)
        reduced_hist_col = initial_hist_col[:max_index]
        initial_hist_col = initial_hist_col[max_index:]

        _plot_wall('%s_%d' % (name, wall_id), reduced_hist_col)

        wall_id += 1


def _plot_wall(name, hist_col):
    # Build Canvas
    # Split canvas in a 6 by 5 grid
    gStyle.SetOptStat(0)

    canvas = TCanvas(name, '')
    canvas.Divide(6, 5)

    # Allocate histograms
    for var_id in range(len(hist_col)):
        var_hist = hist_col[var_id]

        canvas.cd(var_id + 1)
        gPad.SetLogy(1)

        var_hist.Draw('HIST')
        var_hist.Draw('SAME AXIG')

    # Save
    canvas.SaveAs(name + '.png')
