import array
import math

import numpy as np

from muic.pythia.commons.colliders import machines


class ParameterSet(object):

    def __init__(self, machine_name):
        # COMPARISONS
        self.min_q2_dcs = 1e-18
        self.max_q2_dcs = 1e8

        # REACH
        self.desired_sensitivity = 2
        self.desired_luminosities = [
            30, 300, 3000,
            237, 400
        ]
        self.desired_gb = [
            0.01750, 0.00800, 0.00550,  # 500 GeV
            0.01250, 0.00550, 0.00400,  # 350 GeV
            0.00600, 0.00350, 0.00275,  # 200 GeV
        ]
        self.min_gb = 1e-3
        self.max_gb = 0.1
        self.min_delbs = 1e-3
        self.max_delbs = 2

        # # For MUIC COMPARISON
        # self.min_gb = 1e-3
        # self.max_gb = 0.09
        # self.min_delbs = 1e-3
        # self.max_delbs = 2

        # FITS
        self.pscan_min_delbs = 1e-3
        self.pscan_max_delbs = 1e3
        self.pscan_min_mass = 1e2
        self.pscan_max_mass = 1e5
        self.intf_pscan_min_xsec = 1e-1
        self.intf_pscan_max_xsec = 1e4
        self.zp_pscan_min_xsec = 1e-4
        self.zp_pscan_max_xsec = 1e2

        # EFFICIENCIES RELAXED
        self.b_as_b_prob = 0.7
        self.c_as_b_prob = 0.1
        self.l_as_b_prob = 0.01

        # # EFFICIENCIES STRICT
        # self.b_as_b_prob = 0.5
        # self.c_as_b_prob = 0.05
        # self.l_as_b_prob = 0.001

        # UNIFORM
        self.machine = machines[machine_name]
        self.com_energy_squared = 4 * self.machine['Emu'] * self.machine['Ep']
        self.min_q2 = 100
        self.max_q2 = self.com_energy_squared
        self.min_x = 1e-3
        self.max_x = 1
        self.min_y = 1e-2
        self.max_y = 1
        self.q2bins_uniform = np.arange(self.min_q2, 1e5, 10)
        self.xbins_uniform = np.linspace(1e-6, 1, 200)
        self.ybins_uniform = np.linspace(1e-2, 1, 100)
        self.eta_uniform = np.linspace(-10, 4, 70)
        self.angljb_uniform = np.linspace(0, 3.5, 100)

        # Q2 BINS
        self.q2bins = []

        for p in range(8):
            for i in range(1, 10):
                q2 = i * 10 ** p

                if q2 < self.min_q2:
                    continue

                self.q2bins.append(i * 10 ** p)

        self.q2bins = array.array('d', self.q2bins)

        # XBINS
        self.xbins = []

        for p in range(6):
            for i in range(1, 10):
                self.xbins.append(i * math.pow(10, p - 6))

        self.xbins.append(1)

        self.xbins = array.array('d', self.xbins)
