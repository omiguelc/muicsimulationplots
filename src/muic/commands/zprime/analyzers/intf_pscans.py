import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TGraphErrors
from ROOT import TLegend
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kRed

from muic.commands.zprime.styles import serie_styles
from muic.commands.zprime.utils import vev_phi
from muic.core.analyzers import AbstractAnalyzer
from muic.core.plotters.basic import *
from muic.core.root.labels import draw_fancy_label


class IntfPScanAnalyzer(AbstractAnalyzer):

    def __init__(self, parameters):
        super().__init__()

        self.parameters = parameters

        self.datasets = list()

        self.mass_to_series_name = {
            200: '200GeV',
            350: '350GeV',
            500: '500GeV',
            1000: '1TeV',
            3000: '3TeV',
            10000: '10TeV'
        }

        self.delbs_to_series_name = {
            0.01: '0p01',
            0.02: '0p02',
            0.03: '0p03',
            0.05: '0p05',
            0.07: '0p07',
            0.1: '0p1',
            0.5: '0p5',
            1: '1',
            10: '10',
            100: '100'
        }

        self.fit_results = dict()

    def process_entry(self, dataset):
        # UNPACK
        mzp = int(dataset[1])
        delbs = float(dataset[2])
        xsec = float(dataset[3])
        xsec_err = float(dataset[4])

        mzp_series_name = self.mass_to_series_name.get(mzp)

        xsec_vs_delbs_graph_plotter = self.checkout_plotter(
            IntfXSecVsDelbsPlotter,
            'intf_mzp_%s_xsec_vs_delbs' % mzp_series_name,
            '%s @ m_{Z\'} = %d GeV' % (self.parameters.machine['label'], mzp),
            '#delta_{bs}', '#sigma [fb]',
            self.parameters.pscan_min_delbs, self.parameters.pscan_max_delbs,
            self.parameters.intf_pscan_min_xsec, self.parameters.intf_pscan_max_xsec,
            logx=True, logy=True)

        xsec_vs_delbs_graph_plotter.fill(delbs, xsec, xsec_err)

        self.datasets.append(dataset)

    def post_production(self):
        # CALCULATE VALUES
        for mzp, series_name in self.mass_to_series_name.items():
            plotter = self.plotters.get('intf_mzp_%s_xsec_vs_delbs' % series_name)

            if plotter is None:
                continue

            A = plotter.fit_result.Parameter(0)
            A_err = plotter.fit_result.ParError(0)
            B = A / (1.3e-9 * mzp * vev_phi)
            B_err = A_err / (1.3e-9 * mzp * vev_phi)

            self.fit_results[mzp] = {
                'A': A, 'A_err': A_err,
                'B': B, 'B_err': B_err
            }

        # WRITE FIT RESULTS
        with open('intf_pscan_fits.csv', 'w') as f:
            f.write('mzp [GeV],A,A_err,B,B_err\n')

            for mzp, params in self.fit_results.items():
                A = params['A']
                A_err = params['A_err']
                B = params['B']
                B_err = params['B_err']

                f.write('%d,%s,%s,%s,%s\n' % (
                    mzp,
                    '{:e}'.format(A), '{:e}'.format(A_err),
                    '{:e}'.format(B), '{:e}'.format(B_err)))

        # PLOT
        plot_canvas = TCanvas('intf_mzp_delbs', '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1)
        plot_canvas.SetLogy(1)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            self.parameters.pscan_min_delbs, self.parameters.intf_pscan_min_xsec,
            self.parameters.pscan_max_delbs, self.parameters.intf_pscan_max_xsec,
            '%s' % self.parameters.machine['label'])
        frame.GetXaxis().SetTitle('#delta_{bs}')
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetYaxis().SetTitle('#sigma [fb]')
        frame.GetYaxis().SetTitleOffset(1.450)

        legend_x0 = 0.235
        legend_y0 = 0.235

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.425, legend_y0 + 0.275)
        legend.SetMargin(0.250)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)
        legend.SetFillColorAlpha(kBlack, 0.15)

        for mzp, series_name in self.mass_to_series_name.items():
            series_style = serie_styles.get(series_name)

            if series_style is None:
                continue

            plotter = self.plotters.get('intf_mzp_%s_xsec_vs_delbs' % series_name)

            if plotter is None:
                continue

            plotter.draw_function.SetLineColor(serie_styles[series_name]['color'])
            plotter.draw_function.SetLineStyle(serie_styles[series_name]['line-style'])
            plotter.draw_function.SetLineWidth(2)

            legend.AddEntry(plotter.draw_function, serie_styles[series_name]['label'], 'L')

            plotter.draw_function.Draw('SAME')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.235, 0.800)

        plot_canvas.SaveAs('intf_pscan_fits.png')


class IntfXSecVsDelbsPlotter(AbstractPlotter):

    def __init__(self, name, title,
                 x_title, y_title,
                 x_draw_min, x_draw_max,
                 y_draw_min, y_draw_max,
                 logx=False, logy=False):
        super().__init__()

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.n = 0
        self.x_arr = list()
        self.y_arr = list()
        self.y_err_arr = list()
        self.logx = logx
        self.logy = logy

        self.plot = None
        self.fit_result = None
        self.fit_function = None

        self.x_draw_min = x_draw_min
        self.x_draw_max = x_draw_max
        self.y_draw_min = y_draw_min
        self.y_draw_max = y_draw_max

    def fill(self, x, y, y_err):
        self.n += 1
        self.x_arr.append(x)
        self.y_arr.append(y)
        self.y_err_arr.append(y_err)

    def consolidate(self):
        # CREATE
        self.plot = TGraphErrors(
            self.n,
            array.array('d', self.x_arr), array.array('d', self.y_arr),
            np.zeros(len(self.x_arr)), array.array('d', self.y_err_arr))
        self.plot.SetTitle(self.title)

        # FIT
        lowest_y = min(self.y_arr)
        maximum_y = max(self.y_arr) * 2

        self.fit_function = TF1(
            self.name + '_fit', '[0] / x',
            min(self.x_arr) * 0.9, max(self.x_arr) * 1.1)
        self.fit_function.SetParameter(0, lowest_y)
        self.fit_function.SetParLimits(0, 0, maximum_y)

        self.fit_result = self.plot.Fit(
            self.fit_function, '0 M S Q R')

        self.draw_function = TF1(
            self.name + '_fit', '[0] / x',
            self.x_draw_min, self.x_draw_max)
        self.draw_function.SetParameter(0, self.fit_result.Parameter(0))

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(1111)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            self.x_draw_min, self.y_draw_min,
            self.x_draw_max, self.y_draw_max,
            self.title)

        frame.GetXaxis().SetTitle(self.x_title)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetYaxis().SetTitle(self.y_title)
        frame.GetYaxis().SetTitleOffset(1.450)

        self.draw_function.SetLineColor(kRed)
        self.draw_function.SetLineWidth(2)
        self.draw_function.Draw('SAME')

        self.plot.SetMarkerStyle(8)
        self.plot.SetMarkerSize(0.5)
        self.plot.Draw('P')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        plot_canvas.SaveAs(self.name + '.png')
