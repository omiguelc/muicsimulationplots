import ROOT
import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TGraph
from ROOT import TLegend
from ROOT import kBlack
from ROOT import kBlue
from ROOT import kGreen
from ROOT import kMagenta
from ROOT import kOrange
from ROOT import kRed

from muic.commands.zprime.utils import vev, trident_limit_factor
from muic.commands.zprime.utils import vev_phi
from muic.core.analyzers import AbstractAnalyzer
from muic.core.commons.polynomials import find_quadratic_roots
from muic.core.plotters.basic import *
from muic.core.root.labels import draw_fancy_label


class ReachAnalyzer(AbstractAnalyzer):

    def __init__(self, parameters, suffix):
        super().__init__()

        self.parameters = parameters
        self.suffix = suffix

        self.reach_lines = list()
        self.req_lumi_lines = list()

    def process_entry(self, data):
        # UNPACK
        mzp = data['mzp']
        var = data['var']
        B1 = data['B1']
        B2 = data['B2']
        k = data['k']
        cut_info = data['cut_info']
        xsec_list = data['xsec-list']

        C = 1.3e-9 * mzp * vev_phi  # Bs_antiBs & LFUV Constraint Constraint
        C_SM = 1.3e-9 * mzp * vev

        # DEFINE FUNCTIONS
        def dbs_lfuv_limit_rfunction(arr, par):
            return C / arr[0]

        def dbs_trident_limit_rfunction(arr, par):
            return C_SM / arr[0] * trident_limit_factor

        def reach_rfunction(arr, par):
            sig = par[0]
            B1 = par[1]
            B2 = par[2]
            k = par[3]
            gb = arr[0]

            dbs2 = (sig - B1 * gb ** 2 - B2 * gb) / (k * B1 * gb ** 2)

            if dbs2 < 0:
                dbs2 = 0

            return math.sqrt(dbs2)

        # CALCULATE LUMI REQUIRED
        signal_acceptance = cut_info['signal_acceptance']
        bkg_xsec = cut_info['bkg_xsec']

        def calc_trid_limit_on_gb(gb_trid):
            req_sigma = B1 * k * C_SM ** 2 * trident_limit_factor ** 2 + B1 * gb_trid ** 2 + B2 * gb_trid
            req_lumi = (self.parameters.desired_sensitivity / (signal_acceptance * req_sigma)) ** 2
            req_lumi *= (signal_acceptance * req_sigma + bkg_xsec)
            return req_sigma, req_lumi

        for gb_trid in self.parameters.desired_gb:
            req_sigma, req_lumi = calc_trid_limit_on_gb(gb_trid)

            args = (var, '{:0.12e}'.format(gb_trid), req_sigma, req_lumi)

            self.req_lumi_lines.append('%s,%s,%f,%f\n' % args)

        # FIND MIN AND MAX PARAM VALUES
        reach_list = list()
        dataset_gb_lfuv = dict()
        dataset_dbs_lfuv = dict()
        dataset_db_trid = dict()
        dataset_dbs_trid = dict()
        max_gb = None
        min_gb = None
        max_dbs = None
        min_dbs = None

        for dataset in xsec_list:
            intlumi = dataset['intlumi']
            req_sigma = dataset['req_sigma']
            cur_sigma = dataset['cur_sigma']

            gb_lfuv = max(find_quadratic_roots(
                B1, B2, B1 * k * C ** 2 - req_sigma))
            delbs_lfuv = C / gb_lfuv

            cur_gb_lfuv = max(find_quadratic_roots(
                B1, B2, B1 * k * C ** 2 - cur_sigma))
            cur_delbs_lfuv = C / cur_gb_lfuv

            cur_delbs_chk = abs(cur_delbs_lfuv / 0.1 - 1)
            assert cur_delbs_chk < 0.01, 'Current reach cross-check failed'

            gb_trid = max(find_quadratic_roots(
                B1, B2, B1 * k * C_SM ** 2 * trident_limit_factor ** 2 - req_sigma))
            delbs_trid = C_SM / gb_trid * trident_limit_factor

            max_gb = (gb_trid if max_gb is None else max(gb_trid, max_gb))
            min_gb = (gb_lfuv if min_gb is None else min(gb_lfuv, min_gb))
            max_dbs = (delbs_lfuv if max_dbs is None else max(delbs_lfuv, max_dbs))
            min_dbs = (delbs_trid if min_dbs is None else min(delbs_trid, min_dbs))

            dataset_gb_lfuv[intlumi] = gb_lfuv
            dataset_dbs_lfuv[intlumi] = delbs_lfuv
            dataset_db_trid[intlumi] = gb_trid
            dataset_dbs_trid[intlumi] = delbs_trid

            chk_sigma, chk_lumi = calc_trid_limit_on_gb(gb_trid)

            assert abs(1 - chk_sigma / req_sigma) < 0.01, 'TridLimit sigma check failed.'
            assert abs(1 - chk_lumi / intlumi) < 0.01, 'TridLimit lumi check failed.'

        min_gb /= 2
        max_gb *= 2

        min_gb = self.parameters.min_gb
        max_gb = self.parameters.max_gb
        min_dbs = self.parameters.min_delbs
        max_dbs = self.parameters.max_delbs

        # BUILD REACH FUNCTION
        for dataset in xsec_list:
            label = dataset['label']
            intlumi = dataset['intlumi']
            req_sigma = dataset['req_sigma']
            gb_lfuv = dataset_gb_lfuv[intlumi]
            delbs_lfuv = dataset_dbs_lfuv[intlumi]
            gb_trid = dataset_db_trid[intlumi]
            delbs_trid = dataset_dbs_trid[intlumi]

            gb_when_dbs_is_min = max(find_quadratic_roots(
                B1 * (1 + k * min_dbs ** 2), B2, - req_sigma))

            gb_when_dbs_is_max = max(find_quadratic_roots(
                B1 * (1 + k * max_dbs ** 2), B2, - req_sigma))

            reach_function = ROOT.TF1(
                '%d_reach_fun' % intlumi,
                reach_rfunction,
                gb_when_dbs_is_max * 0.9,
                gb_when_dbs_is_min * 1.1,
                4)  # Number of parameters needed
            reach_function.SetParameter(0, req_sigma)
            reach_function.SetParameter(1, B1)
            reach_function.SetParameter(2, B2)
            reach_function.SetParameter(3, k)

            reach_list.append((
                label, reach_function, intlumi, req_sigma, gb_lfuv, delbs_lfuv, gb_trid, delbs_trid))

        # BUILD BS MIXING LIMIT FUNCTION
        lfuv_limit_function = TF1(
            'lfuv_limit_fun',
            dbs_lfuv_limit_rfunction,
            min_gb / 2, max_gb * 2, 0)

        # BUILD TRIDENT MIXING LIMIT FUNCTION
        trident_limit_function = TF1(
            'trident_limit_fun',
            dbs_trident_limit_rfunction,
            min_gb / 2, max_gb * 2, 0)

        # PLOT
        plot_canvas = TCanvas('%s_reach' % var, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1)
        plot_canvas.SetLogy(1)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            min_gb, min_dbs,
            max_gb, max_dbs,
            '%s @ m_{Z\'}=%d GeV' % (self.parameters.machine['label'], mzp))
        frame.GetXaxis().SetTitle('g_{b}')
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetNdivisions(5)
        frame.GetYaxis().SetTitle('#delta_{bs}')
        frame.GetYaxis().SetTitleOffset(1.450)

        legend_x0 = 0.235
        legend_y0 = 0.235

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.450, legend_y0 + 0.275)
        legend.SetMargin(0.250)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)
        legend.SetFillColorAlpha(kBlack, 0.15)

        n_points_limits = 25000
        limit_x = np.linspace(min_gb / 2, max_gb * 2, n_points_limits)

        ll_y = np.asarray([lfuv_limit_function.Eval(x) for x in limit_x], 'd')
        lfuv_limit_graph = TGraph(n_points_limits, limit_x, ll_y)

        lfuv_limit_graph.SetLineColor(kBlue)
        lfuv_limit_graph.SetLineWidth(9002)  # previously 2
        lfuv_limit_graph.SetFillStyle(1001)
        lfuv_limit_graph.SetFillColorAlpha(kBlue, 0.1)
        lfuv_limit_graph.Draw('SAME')

        legend.AddEntry(lfuv_limit_graph, 'B_{s}-#bar{B_{s}} Mixing Limit', 'L')

        tl_y = np.asarray([trident_limit_function.Eval(x) for x in limit_x], 'd')
        trident_limit_graph = TGraph(n_points_limits, limit_x, tl_y)

        trident_limit_graph.SetLineColor(kRed)
        trident_limit_graph.SetLineWidth(-9002)
        trident_limit_graph.SetFillStyle(1001)
        trident_limit_graph.SetFillColorAlpha(kRed, 0.1)
        trident_limit_graph.Draw('SAME')

        legend.AddEntry(trident_limit_graph, 'Neutrino Trident Limit', 'L')

        colors = [kRed + 2, kRed + 2, kGreen - 2, kOrange - 2, kMagenta]

        for reach in reach_list:
            label = reach[0]
            function = reach[1]

            function.SetLineColor(colors.pop())
            function.SetLineWidth(2)
            function.SetNpx(30000)

            legend.AddEntry(function, label, 'L')

            function.Draw('SAME')

        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        legend.Draw()

        draw_fancy_label(0.235, 0.800)

        plot_canvas.SaveAs('%s_reach_%s.png' % (var, self.suffix))

        # APPEND REACH
        for reach in reach_list:
            intlumi = reach[2]
            xsec = reach[3]
            gb_lfuv = reach[4]
            delbs_lfuv = reach[5]
            gb_trid = reach[6]
            delbs_trid = reach[7]

            args = (
                var, intlumi, xsec,
                '{:0.12e}'.format(gb_lfuv),
                '{:0.12e}'.format(delbs_lfuv),
                '{:0.12e}'.format(gb_trid),
                '{:0.12e}'.format(delbs_trid))

            self.reach_lines.append('%s,%d,%f,%s,%s,%s,%s\n' % args)

    def post_production(self):
        with open('req_lumi_%s.csv' % self.suffix, 'w') as f:
            f.write('var,gb_trid,xsec [fb],intlumi [1/fb]\n')

            for line in self.req_lumi_lines:
                f.write(line)

        with open('reach_%s.csv' % self.suffix, 'w') as f:
            f.write('var,intlumi [1/fb],xsec [fb],gb_lfuv,delbs_lfuv,gb_trid,delbs_trid\n')

            for line in self.reach_lines:
                f.write(line)
