from ROOT import TCanvas
from ROOT import TH1D
from ROOT import THStack
from ROOT import TLegend
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kFullDotLarge
from ROOT import kWhite

from muic.commands.zprime.styles import serie_styles
from muic.core.analyzers import AbstractAnalyzer
from muic.core.commons.polynomials import find_quadratic_roots
from muic.core.plotters.basic import *
from muic.core.root.labels import draw_fancy_label


class SensitivityAnalyzer(AbstractAnalyzer):

    def __init__(self, parameters):
        super().__init__()

        self.parameters = parameters

        self.dataset_xsec = dict()
        self.var_min_cut_info = dict()
        self.req_xsec_min_cut = dict()
        self.cur_xsec_min_cut = dict()

        self.agg_plot_members = {
            'Q2_dcs': dict(),
            'Q2_dcs_ubin': dict(),
            'x_dcs': dict(),
            'x_dcs_ubin': dict(),
            'y_dcs': dict(),
            'eta_mu_dcs': dict(),
            'eta_had_dcs': dict()
        }

        self.agg_plot_styles = {
            'Q2_dcs': {'x_low': parameters.min_q2, 'x_up': parameters.max_q2,
                       'y_low': parameters.min_q2_dcs, 'y_up': parameters.max_q2_dcs,
                       'logx': 1, 'logy': 1},
            'x_dcs': {'x_low': parameters.min_x, 'x_up': parameters.max_x, 'y_low': 1e-12, 'y_up': 1e12, 'logx': 1,
                      'logy': 1},
            'y_dcs': {'x_low': parameters.min_y, 'x_up': parameters.max_y, 'y_low': 1e-12, 'y_up': 1e8, 'logx': 0,
                      'logy': 1},
            'eta_mu_dcs': {'x_low': -10, 'x_up': 4, 'y_low': 1e-18, 'y_up': 1e12, 'logx': 0, 'logy': 1},
            'eta_had_dcs': {'x_low': -10, 'x_up': 4, 'y_low': 1e-18, 'y_up': 1e12, 'logx': 0, 'logy': 1},
        }

    def process_entry(self, data):
        # UNPACK
        name = data[0]
        root_file = data[1]

        # REGISTER
        for entry in root_file.meta:
            self.dataset_xsec[name] = {
                'sigma': entry.gen_sigma * 1e-3,  # fb -> pb
                'sigma_error': entry.gen_sigma_error * 1e-3,  # fb -> pb
                'processed_sigma': entry.processed_sigma * 1e-3,  # fb -> pb
                'processed_sigma_error': entry.processed_sigma_error * 1e-3,  # fb -> pb
            }

        # CLASSIFY
        for plot_name, members in self.agg_plot_members.items():
            # GET PLOT
            plot = getattr(root_file, plot_name)

            if plot is not None:
                plot = plot.Clone(name + '-' + plot_name)

            # ADD TO GROUP
            members[name] = plot

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)

        # GET XSEC
        generator_bkg_xsec = self.dataset_xsec['nc-b-jet']['sigma'] \
                             + self.dataset_xsec['nc-c-jet']['sigma'] \
                             + self.dataset_xsec['nc-l-jet']['sigma']

        generator_bkg_xsec_err = math.sqrt(
            self.dataset_xsec['nc-b-jet']['sigma_error'] ** 2
            + self.dataset_xsec['nc-c-jet']['sigma_error'] ** 2
            + self.dataset_xsec['nc-l-jet']['sigma_error'] ** 2)

        generator_signal_xsec = self.dataset_xsec['zprime']['sigma'] \
                                + self.dataset_xsec['interference']['sigma']

        generator_signal_xsec_err = math.sqrt(
            self.dataset_xsec['zprime']['sigma_error'] ** 2
            + self.dataset_xsec['interference']['sigma_error'] ** 2)

        generator_signal_xsec = generator_signal_xsec
        generator_signal_xsec_err = generator_signal_xsec_err
        generator_bkg_xsec = generator_bkg_xsec
        generator_bkg_xsec_err = generator_bkg_xsec_err

        # SENSITIVITY VAR CUTS
        sensitivity_output_lines = list()

        var_plots = [
            ('Q2', 'Q^{2}_{min} [GeV^{2}]', self.parameters.q2bins_uniform, 'Q2_dcs_ubin', True),
            ('x', 'x_{min}', self.parameters.xbins_uniform, 'x_dcs_ubin', True),
            ('y', 'y_{min}', self.parameters.ybins_uniform, 'y_dcs', True),
            ('eta_mu', '#eta_{mu,min}', self.parameters.eta_uniform, 'eta_mu_dcs', False),
            ('eta_had', '#eta_{had,min}', self.parameters.eta_uniform, 'eta_had_dcs', False),
        ]

        for var_plot in var_plots:
            var_name = var_plot[0]
            var_label = var_plot[1]
            sensitivity_edges = var_plot[2]
            sensitivity_plots = var_plot[3]
            use_logx = var_plot[4]

            # GET PLOTS
            agg_plot_members = self.agg_plot_members[sensitivity_plots]

            # INIT XSEC PLOTS
            n_sensitivity_bins = len(sensitivity_edges) - 1

            signal_xsec_1d_plot = TH1D(
                'signal_xsec_1d',
                'Signal Cross Section', n_sensitivity_bins,
                sensitivity_edges)
            bkg_xsec_1d_plot = TH1D(
                'bkg_xsec_1d',
                'Background Cross Section', n_sensitivity_bins,
                sensitivity_edges)
            nc_bj_xsec_1d_plot = TH1D(
                'nc_bj_xsec_1d',
                'NC b-jet Cross Section', n_sensitivity_bins,
                sensitivity_edges)
            nc_cj_xsec_1d_plot = TH1D(
                'nc_cj_xsec_1d',
                'NC c-jet Cross Section', n_sensitivity_bins,
                sensitivity_edges)
            nc_lj_xsec_1d_plot = TH1D(
                'nc_lj_xsec_1d',
                'NC l-jet Cross Section', n_sensitivity_bins,
                sensitivity_edges)

            # Find last bin
            dataset_next_xsec = dict()
            dataset_last_bin_id = dict()

            for dataset_name, meta in self.dataset_xsec.items():
                sigma = meta['processed_sigma']

                if dataset_name == 'zprime':
                    sigma = sigma * self.parameters.b_as_b_prob
                elif dataset_name == 'interference':
                    sigma = sigma * self.parameters.b_as_b_prob
                elif dataset_name == 'nc-b-jet':
                    sigma = sigma * self.parameters.b_as_b_prob
                elif dataset_name == 'nc-c-jet':
                    sigma = sigma * self.parameters.c_as_b_prob
                elif dataset_name == 'nc-l-jet':
                    sigma = sigma * self.parameters.l_as_b_prob

                dataset_next_xsec[dataset_name] = sigma

            for bin_id in range(n_sensitivity_bins):
                bin_id = bin_id + 1

                for dataset_name, series in agg_plot_members.items():
                    dcs = series.GetBinContent(bin_id)

                    if dcs == 0:
                        continue

                    dataset_last_bin_id[dataset_name] = bin_id

            # SUBTRACT PARTIAL XSEC FOR UNDERFLOW BIN
            for dataset_name, series in agg_plot_members.items():
                # Underflow Bin
                bin_id = 0

                # Get Bin Info
                bin_width = series.GetXaxis().GetBinWidth(bin_id)

                # Get Cross-sections
                next_int_xsec = dataset_next_xsec.get(dataset_name, 0)
                dcs = series.GetBinContent(bin_id)
                pcs = dcs * bin_width

                if dataset_name == 'zprime':
                    pcs = pcs * self.parameters.b_as_b_prob
                elif dataset_name == 'interference':
                    pcs = pcs * self.parameters.b_as_b_prob
                elif dataset_name == 'nc-b-jet':
                    pcs = pcs * self.parameters.b_as_b_prob
                elif dataset_name == 'nc-c-jet':
                    pcs = pcs * self.parameters.c_as_b_prob
                elif dataset_name == 'nc-l-jet':
                    pcs = pcs * self.parameters.l_as_b_prob

                dataset_next_xsec[dataset_name] = next_int_xsec - pcs

            # SUBTRACT PARTIAL XSEC FOR EACH BIN
            for cut_bin in range(n_sensitivity_bins):
                cut_min_value = sensitivity_edges[cut_bin]
                cut_bin_center = (sensitivity_edges[cut_bin] + sensitivity_edges[cut_bin + 1]) / 2

                for dataset_name, series in agg_plot_members.items():
                    bin_id = cut_bin + 1

                    # Short-Circuit
                    last_bin_id = dataset_last_bin_id.get(dataset_name, 0)

                    if last_bin_id < bin_id:
                        continue

                    # Get Bin Info
                    bin_width = series.GetXaxis().GetBinWidth(bin_id)
                    bin_low = series.GetXaxis().GetBinLowEdge(bin_id)

                    assert cut_min_value == bin_low, 'Bins don\'t line-up'

                    # Get Cross-sections
                    next_int_xsec = dataset_next_xsec.get(dataset_name, 0)
                    dcs = series.GetBinContent(bin_id)
                    pcs = dcs * bin_width

                    if dataset_name == 'zprime':
                        signal_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        pcs = pcs * self.parameters.b_as_b_prob
                    elif dataset_name == 'interference':
                        signal_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        pcs = pcs * self.parameters.b_as_b_prob
                    elif dataset_name == 'nc-b-jet':
                        nc_bj_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        bkg_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        pcs = pcs * self.parameters.b_as_b_prob
                    elif dataset_name == 'nc-c-jet':
                        nc_cj_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        bkg_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        pcs = pcs * self.parameters.c_as_b_prob
                    elif dataset_name == 'nc-l-jet':
                        nc_lj_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        bkg_xsec_1d_plot.Fill(cut_bin_center, next_int_xsec)
                        pcs = pcs * self.parameters.l_as_b_prob

                    dataset_next_xsec[dataset_name] = next_int_xsec - pcs

            # CALCULATE ACCEPTANCE
            bkg_acceptance_1d_plotter = self.checkout_plotter(
                Hist1DPlotter,
                '%s_bkg_acceptance_min_cut_1d' % var_name,
                '%s' % (self.parameters.machine['label']),
                var_label, 'Acceptance [%]',
                xbins=sensitivity_edges,
                logx=use_logx, logy=False)

            signal_acceptance_1d_plotter = self.checkout_plotter(
                Hist1DPlotter,
                '%s_signal_acceptance_min_cut_1d' % var_name,
                '%s' % (self.parameters.machine['label']),
                var_label, 'Acceptance [%]',
                xbins=sensitivity_edges,
                logx=use_logx, logy=False)

            for bin_id in range(n_sensitivity_bins):
                bin_id = bin_id + 1

                signal_xsec = signal_xsec_1d_plot.GetBinContent(bin_id)
                signal_xsec_err = signal_xsec_1d_plot.GetBinError(bin_id)
                bkg_xsec = bkg_xsec_1d_plot.GetBinContent(bin_id)
                bkg_xsec_err = bkg_xsec_1d_plot.GetBinError(bin_id)

                if generator_bkg_xsec > 0 and bkg_xsec > 0:
                    acceptance = bkg_xsec / generator_bkg_xsec
                    acceptance_err = acceptance * math.sqrt(
                        (bkg_xsec_err / bkg_xsec) ** 2
                        + (generator_bkg_xsec_err / generator_bkg_xsec) ** 2
                    )

                    bkg_acceptance_1d_plotter.plot.SetBinContent(bin_id, acceptance * 100)
                    bkg_acceptance_1d_plotter.plot.SetBinError(bin_id, acceptance_err * 100)

                if generator_signal_xsec > 0 and signal_xsec > 0:
                    acceptance = signal_xsec / generator_signal_xsec
                    acceptance_err = acceptance * math.sqrt(
                        (signal_xsec_err / signal_xsec) ** 2
                        + (generator_signal_xsec_err / generator_signal_xsec) ** 2
                    )

                    signal_acceptance_1d_plotter.plot.SetBinContent(bin_id, acceptance * 100)
                    signal_acceptance_1d_plotter.plot.SetBinError(bin_id, acceptance_err * 100)

            # CALCULATE SENSITIVITY
            max_sensitivity = None
            signal_xsec_at_max = None
            bkg_xsec_at_max = None
            x_cut_at_max = None
            bin_at_max = None

            sensitivity_1d_plot = TH1D(
                'sensitivity_1d', 'Sensitivity',
                n_sensitivity_bins, sensitivity_edges)

            for bin_id in range(n_sensitivity_bins):
                bin_id = bin_id + 1

                signal_xsec = signal_xsec_1d_plot.GetBinContent(bin_id)
                bkg_xsec = bkg_xsec_1d_plot.GetBinContent(bin_id)

                total_xsec = max(bkg_xsec - signal_xsec, 0)

                if total_xsec <= 0:
                    continue

                sensitivity = signal_xsec / math.sqrt(signal_xsec + bkg_xsec)
                sensitivity_1d_plot.SetBinContent(bin_id, sensitivity)

                if max_sensitivity is None or max_sensitivity < sensitivity:
                    max_sensitivity = sensitivity
                    x_cut_at_max = sensitivity_1d_plot.GetBinLowEdge(bin_id)
                    signal_xsec_at_max = signal_xsec
                    bkg_xsec_at_max = bkg_xsec
                    bin_at_max = bin_id

            x_min = min(sensitivity_edges)
            x_max = max(sensitivity_edges)

            assert x_min <= x_cut_at_max <= x_max

            # GET ACCEPTANCE AT MAX
            signal_acceptance_at_max = signal_acceptance_1d_plotter.plot.GetBinContent(
                bin_at_max) / 100  # Remove percentage
            bkg_acceptance_at_max = bkg_acceptance_1d_plotter.plot.GetBinContent(bin_at_max) / 100  # Remove percentage

            # SAVE CUT INFO
            self.var_min_cut_info[var_name] = {
                'signal_xsec': signal_xsec_at_max * 1e3,  # pb -> fb
                'bkg_xsec': bkg_xsec_at_max * 1e3,  # pb -> fb
                'signal_acceptance': signal_acceptance_at_max,
                'bkg_acceptance': bkg_acceptance_at_max
            }

            # SCALE TO LUMI
            self.req_xsec_min_cut[var_name] = dict()
            self.cur_xsec_min_cut[var_name] = dict()

            for int_luminosity_inv_fb in self.parameters.desired_luminosities:
                int_luminosity_inv_pb = int_luminosity_inv_fb * 1e3  # fb^-1 -> pb^-1
                sqrt_int_luminosity_inv_pb = math.sqrt(int_luminosity_inv_pb)  # fb^-1 * 1e3 -> pb^-1

                # EXTRAPOLATE SENSITIVITY
                sensitivity_1d_plotter = self.checkout_plotter(
                    Hist1DPlotter,
                    '%s_sensitivity_%d_min_cut_1d' % (
                        var_name, int_luminosity_inv_fb),
                    '%s @ L=%d fb^{-1}' % (
                        self.parameters.machine['label'], int_luminosity_inv_fb),
                    var_label, 'S / #sqrt{S + B}',
                    xbins=sensitivity_edges,
                    logx=use_logx, logy=False)

                for bin_id in range(n_sensitivity_bins):
                    bin_id = bin_id + 1

                    sensitivity = sensitivity_1d_plot.GetBinContent(bin_id)
                    sensitivity_1d_plotter.plot.SetBinContent(bin_id, sensitivity * sqrt_int_luminosity_inv_pb)

                max_sensitivity = sensitivity_1d_plotter.plot.GetBinContent(bin_at_max)

                # CALCULATE EVENTS
                signal_evt_1d_plot = signal_xsec_1d_plot.Clone('signal_evt_1d')
                nc_bj_evt_1d_plot = nc_bj_xsec_1d_plot.Clone('nc_bj_evt_1d')
                nc_cj_evt_1d_plot = nc_cj_xsec_1d_plot.Clone('nc_cj_evt_1d')
                nc_lj_evt_1d_plot = nc_lj_xsec_1d_plot.Clone('nc_lj_evt_1d')

                signal_evt_1d_plot.Scale(int_luminosity_inv_pb)
                nc_bj_evt_1d_plot.Scale(int_luminosity_inv_pb)
                nc_cj_evt_1d_plot.Scale(int_luminosity_inv_pb)
                nc_lj_evt_1d_plot.Scale(int_luminosity_inv_pb)

                # CALCULATE REQUIRED SIGMA
                def calc_req_sigma(sensitivity):
                    aX2 = signal_acceptance_at_max ** 2 * int_luminosity_inv_fb ** 2
                    bX = -sensitivity ** 2 * signal_acceptance_at_max * int_luminosity_inv_fb
                    c = -sensitivity ** 2 * bkg_xsec_1d_plot.GetBinContent(bin_at_max) * int_luminosity_inv_pb

                    req_sigma1, req_sigma2 = find_quadratic_roots(aX2, bX, c)

                    return max(0, req_sigma1, req_sigma2)

                cur_sigma = calc_req_sigma(max_sensitivity)
                cur_sigma_chk = abs(cur_sigma / (generator_signal_xsec * 1e3) - 1)

                assert cur_sigma_chk < 0.01, 'Current sigma cross-check failed'

                req_sigma = calc_req_sigma(self.parameters.desired_sensitivity)

                # SAVE XSEC
                self.req_xsec_min_cut[var_name][int_luminosity_inv_fb] = req_sigma
                self.cur_xsec_min_cut[var_name][int_luminosity_inv_fb] = cur_sigma

                # APPEND OUTPUT LINE
                args = (var_name, int_luminosity_inv_fb,
                        max_sensitivity, x_cut_at_max,
                        signal_xsec_at_max * 1e3, bkg_xsec_at_max * 1e3,
                        '{:0.12e}'.format(signal_acceptance_at_max * 100),
                        '{:0.12e}'.format(bkg_acceptance_at_max * 100),
                        req_sigma)

                sensitivity_output_lines.append('%s,%d,%f,%f,%f,%f,%s,%s,%f\n' % args)

                # PLOT EVENTS
                plot_canvas = TCanvas('', '')
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(use_logx)
                plot_canvas.SetLogy(1)
                plot_canvas.cd()

                y_min = min(
                    signal_evt_1d_plot.GetMinimum(0),
                    nc_bj_evt_1d_plot.GetMinimum(0) +
                    nc_cj_evt_1d_plot.GetMinimum(0) +
                    nc_lj_evt_1d_plot.GetMinimum(0))

                y_max = max(
                    signal_evt_1d_plot.GetMaximum(),
                    nc_bj_evt_1d_plot.GetMaximum() +
                    nc_cj_evt_1d_plot.GetMaximum() +
                    nc_lj_evt_1d_plot.GetMaximum()) * 1.1

                frame = plot_canvas.DrawFrame(
                    x_min, y_min, x_max, y_max,
                    '%s @ L=%d fb^{-1}' % (self.parameters.machine['label'], int_luminosity_inv_fb))
                frame.GetXaxis().SetTitle(var_label)
                frame.GetXaxis().SetTitleOffset(1.350)
                frame.GetYaxis().SetTitle('a.u.')
                frame.GetYaxis().SetTitleOffset(1.450)

                legend_x0 = 0.235
                legend_y0 = 0.215

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.350, legend_y0 + 0.200)
                legend.SetMargin(0.250)
                legend.SetFillColorAlpha(kWhite, 0.3)
                # legend.SetFillStyle(0)

                bkg_1d_plot = THStack('%s_bkg_%d_min_cut_1d' % (var_name, int_luminosity_inv_fb), '')
                nc_bj_evt_1d_plot.SetLineColorAlpha(serie_styles['nc-b-jet']['color'], 1.0)
                nc_cj_evt_1d_plot.SetLineColorAlpha(serie_styles['nc-c-jet']['color'], 1.0)
                nc_lj_evt_1d_plot.SetLineColorAlpha(serie_styles['nc-l-jet']['color'], 1.0)
                nc_bj_evt_1d_plot.SetFillColorAlpha(serie_styles['nc-b-jet']['color'] - 4, 1.0)
                nc_cj_evt_1d_plot.SetFillColorAlpha(serie_styles['nc-c-jet']['color'] - 4, 1.0)
                nc_lj_evt_1d_plot.SetFillColorAlpha(serie_styles['nc-l-jet']['color'] - 4, 1.0)
                bkg_1d_plot.Add(nc_bj_evt_1d_plot, 'HIST')
                bkg_1d_plot.Add(nc_cj_evt_1d_plot, 'HIST')
                bkg_1d_plot.Add(nc_lj_evt_1d_plot, 'HIST')
                bkg_1d_plot.Draw('SAME')

                signal_evt_1d_plot.SetLineColorAlpha(serie_styles['signal']['color'], 1.0)
                signal_evt_1d_plot.SetFillColorAlpha(serie_styles['signal']['color'] - 4, 1.0)
                signal_evt_1d_plot.Draw('SAME HIST')

                legend.AddEntry(signal_evt_1d_plot, serie_styles['signal']['label'], 'F')
                legend.AddEntry(nc_bj_evt_1d_plot, serie_styles['nc-b-jet']['label'], 'F')
                legend.AddEntry(nc_cj_evt_1d_plot, serie_styles['nc-c-jet']['label'], 'F')
                legend.AddEntry(nc_lj_evt_1d_plot, serie_styles['nc-l-jet']['label'], 'F')

                frame.Draw('SAME AXIS')
                frame.Draw('SAME AXIG')

                legend.Draw()

                draw_fancy_label(0.235, 0.800)

                plot_canvas.SaveAs('%s_events_%d_min_cut_1d.png' % (var_name, int_luminosity_inv_fb))

        # WRITE SENSITIVITY RESULTS
        if len(sensitivity_output_lines) > 0:
            with open('sensitivity_min_cut.csv', 'w') as f:
                f.write('var_name,int_lumi_fb,'
                        'max_sensitivity,val_at_max,'
                        'signal_xsec_at_max [fb],bkg_xsec_at_max [fb],'
                        'signal_acceptance_at_max [%],bkg_acceptance_at_max [%],'
                        'req_sigma [fb]\n')

                for line in sensitivity_output_lines:
                    f.write(line)

        # DCS-VAR COMPARISON
        var_plots = [
            ('Q2', 'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
             self.parameters.q2bins, 'Q2_dcs'),
            ('x', 'x', 'd#sigma / dx [pb]',
             self.parameters.xbins, 'x_dcs'),
            ('y', 'y', 'd#sigma / dy [pb]',
             self.parameters.ybins_uniform, 'y_dcs'),
            ('eta_mu', '#eta_{mu}', 'd#sigma / d#eta_{mu} [pb]',
             self.parameters.eta_uniform, 'eta_mu_dcs'),
            ('eta_had', '#eta_{had}', 'd#sigma / d#eta_{had} [pb]',
             self.parameters.eta_uniform, 'eta_had_dcs'),
        ]

        for var_plot in var_plots:
            var_name = var_plot[0]
            x_title = var_plot[1]
            y_title = var_plot[2]
            plot_bins = var_plot[3]
            plot_group = var_plot[4]

            agg_plot_members = self.agg_plot_members[plot_group]
            agg_plot_style = self.agg_plot_styles[plot_group]

            plot_canvas = TCanvas(var_name + '_comparison', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.SetLogx(agg_plot_style['logx'])
            plot_canvas.SetLogy(agg_plot_style['logy'])
            plot_canvas.cd()

            frame = plot_canvas.DrawFrame(
                agg_plot_style['x_low'], agg_plot_style['y_low'],
                agg_plot_style['x_up'], agg_plot_style['y_up'],
                self.parameters.machine['label'])
            frame.GetXaxis().SetTitle(x_title)
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle(y_title)
            frame.GetYaxis().SetTitleOffset(1.450)

            legend_x0 = 0.235
            legend_y0 = 0.200

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.250, legend_y0 + 0.200)
            legend.SetMargin(0.250)
            legend.SetFillStyle(0)

            plot_copies = []

            for dataset_name, series in agg_plot_members.items():
                style = serie_styles.get(dataset_name)

                if style is None:
                    continue

                series = series.Clone(dataset_name + '_temp')
                plot_copies.append(series)

                if dataset_name == 'zprime':
                    series.Scale(self.parameters.b_as_b_prob)
                if dataset_name == 'interference':
                    series.Scale(self.parameters.b_as_b_prob)
                elif dataset_name == 'nc-b-jet':
                    series.Scale(self.parameters.b_as_b_prob)
                elif dataset_name == 'nc-c-jet':
                    series.Scale(self.parameters.c_as_b_prob)
                elif dataset_name == 'nc-l-jet':
                    series.Scale(self.parameters.l_as_b_prob)

                series.SetMarkerStyle(style['marker'])
                series.SetMarkerColor(style['color'])
                series.SetMarkerSize(style['marker-size'])

                legend.AddEntry(series, style['label'], 'P')

                series.Draw('SAME HIST P E X0')

            # TOTAL PLOT
            total_plot = TH1D('total_1d', 'Total', len(plot_bins) - 1, plot_bins)

            for bin_id in range(total_plot.GetNcells()):
                total_val = 0
                total_err2 = 0

                for dataset_name, series in agg_plot_members.items():
                    val = series.GetBinContent(bin_id)
                    error = series.GetBinError(bin_id)

                    total_val += val
                    total_err2 += error ** 2

                total_plot.SetBinContent(bin_id, total_val)
                total_plot.SetBinError(bin_id, math.sqrt(total_err2))

            total_plot.SetMarkerStyle(kFullDotLarge)
            total_plot.SetMarkerColor(kBlack)
            total_plot.SetMarkerSize(0.5)

            total_plot.Draw('SAME HIST P E X0')

            legend.AddEntry(total_plot, 'Total', 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            legend.Draw()

            draw_fancy_label(0.235, 0.800)

            plot_canvas.SaveAs('%s_comparison.png' % var_name)
