import csv
import json
import math
import os

import ROOT
from ROOT import TFile
from ROOT import TH1
from ROOT import TTree
from ROOT import addressof
from ROOT import gROOT
from ROOT import gStyle
from ROOT import kFALSE
from ROOT import kTRUE

from muic.commands.zprime.analyzers.comparisons import ComparisonAnalyzer
from muic.commands.zprime.analyzers.intf_pscans import IntfPScanAnalyzer
from muic.commands.zprime.analyzers.pythia import PythiaAnalyzer
from muic.commands.zprime.analyzers.reach import ReachAnalyzer
from muic.commands.zprime.analyzers.sensitivity import SensitivityAnalyzer
from muic.commands.zprime.analyzers.zp_pscans import ZPrimePScanAnalyzer
from muic.commands.zprime.parameters import ParameterSet
from muic.core.root.cms import apply_tdr_style
from muic.pythia.commons import detector


def configure_parser(parser):
    parser.add_argument('--machine', dest='machine',
                        default='muic', type=str,
                        help='Specifies the machine being studied (MuIC, MuIC2, LHmuC)')

    parser.add_argument('--individual', dest='individual_en', action='store_const',
                        const=True, default=False,
                        help='Analyzes a simulation')

    parser.add_argument('--signal', dest='signal_en', action='store_const',
                        const=True, default=False,
                        help='Analyzes signal')

    parser.add_argument('--comparison', dest='comparison_en', action='store_const',
                        const=True, default=False,
                        help='Analyzes various simulations')

    parser.add_argument('study', metavar='STUDY',
                        default='study', type=str,
                        help='Name of the Study')

    parser.add_argument('datasets', metavar='DATASETS',
                        default=[], type=str, nargs='+',
                        help='List of datasets')


def run(args):
    # STUPID ROOT STUFF
    gROOT.SetBatch(kTRUE)
    TH1.AddDirectory(kFALSE)
    gROOT.ProcessLine("gErrorIgnoreLevel = kFatal;")

    # STUPID STYLE STUFF
    apply_tdr_style()
    gStyle.SetOptTitle(1)
    gStyle.SetPadTopMargin(0.100)
    gStyle.SetPadBottomMargin(0.100)
    gStyle.SetPadGridX(1)
    gStyle.SetPadGridY(1)

    # GET CONFIG
    parameters = ParameterSet(args.machine)

    # RUN
    if len(args.datasets) == 0:
        print('No datasets given. Aborting')
    elif args.individual_en:
        study = args.study
        request = json.loads(args.datasets[0])

        # PROCESS ALL SIMULATIONS
        os.makedirs('root_files', exist_ok=True)
        os.makedirs(study, exist_ok=True)
        os.chdir(study)

        analyze_pythia(study, request, parameters, '../root_files')

        os.chdir('..')
    elif args.signal_en:
        # PARSE
        study = args.study
        request = json.loads(args.datasets[0])

        # PROCESS ROOT FILES
        os.makedirs(study, exist_ok=True)
        os.chdir(study)

        analyze_sensitivity(study, request, parameters)

        os.chdir('..')
    elif args.comparison_en:
        # PARSE
        study = args.study
        request = json.loads(args.datasets[0])

        # PROCESS ROOT FILES
        os.makedirs(study, exist_ok=True)
        os.chdir(study)

        analyze_comparison(study, request, parameters)

        os.chdir('..')

    print('Done', args)


def analyze_pythia(study, request, parameters, root_file_output_dir, event_limit=-1, use_upper_energy_cut=True):
    analyzer = PythiaAnalyzer(parameters)

    # LOOP DATASETS
    dataset_id = 0

    gen_sigma = 0
    gen_sigma_error = 0

    available_sigma = 0
    available_sigma_error = 0

    processed_n = 0
    processed_sigma = 0
    processed_sigma_error = 0

    for dataset in request['datasets']:
        # UNPACK DATASET
        dataset_input = os.path.join('..', dataset['input'])
        dataset_sigma = dataset['sigma']
        dataset_sigma_error = dataset['sigma_error']
        dataset_low_cutoff = dataset['min_q2']
        dataset_up_cutoff = dataset['max_q2']

        if type(dataset_sigma) == list and type(dataset_sigma_error) == list:
            dataset_sigma = sum(dataset_sigma)
            dataset_sigma_error = math.sqrt(sum(x ** 2 for x in dataset_sigma_error))

        gen_sigma += dataset_sigma * 1e12
        gen_sigma_error += (dataset_sigma_error * 1e12) ** 2

        # LOG
        print('Reading: %s' % dataset_input)

        # OPEN
        root_file = TFile.Open(dataset_input, 'READ')

        # READ METADATA
        meta = {
            '__id__': dataset_id,
            'events_total': 0,
            'events_processed': 0,
            'events_total_weighted': 0,
            'events_processed_weighted': 0,
            'processes': list(),
            'sigma': -1,
            'sigma_error': -1,
            'processed_sigma': -1,
            'processed_sigma_error': -1,
            'weight_sum': -1
        }

        for entry in root_file.meta:
            # GET SIM DATA
            meta['events_total'] = entry.events_total
            meta['events_processed'] = entry.events_processed
            meta['events_total_weighted'] = entry.events_total_weighted
            meta['events_processed_weighted'] = entry.events_processed_weighted

            # GET SIGMA
            pythia_sigma = entry.sigma
            pythia_sigma_error = entry.sigma_error

            if dataset_sigma is not None and dataset_sigma_error is not None:
                pythia_ratio = entry.sigma / dataset_sigma
                pythia_sigma_error = dataset_sigma_error * pythia_ratio

            meta['sigma'] = pythia_sigma
            meta['sigma_error'] = pythia_sigma_error
            meta['processed_sigma'] = pythia_sigma * entry.events_processed_weighted / entry.weight_sum
            meta['processed_sigma_error'] = pythia_sigma_error * entry.events_processed_weighted / entry.weight_sum

            # GET WEIGHT SUM
            meta['weight_sum'] = entry.weight_sum

            # GET PROCESSES
            vsize_proc = entry.vsize_proc

            for i_proc in range(vsize_proc):
                meta['processes'].append({
                    'name': entry.proc[i_proc],
                    'sigma': entry.proc_sigma[i_proc],
                    'sigma_error': entry.proc_sigma_error[i_proc],
                    'processed_sigma': entry.proc_sigma[i_proc] * entry.events_processed_weighted
                                       / entry.weight_sum,
                    'processed_sigma_error': entry.proc_sigma_error[i_proc] * entry.events_processed_weighted
                                             / entry.weight_sum
                })

        # SHOW META
        print('Events (Generator): ' + '{:f}'.format(meta['events_total']))
        print('Weight Sum (Generator): ' + '{:f}'.format(meta['weight_sum']))
        print('Weight Sum (Generator-Crosscheck): ' + '{:f}'.format(meta['events_total_weighted']))
        print('Reported Sigma (Pythia) [fb]: ' + '{:.12e}'.format(meta['sigma'] * 1e12))  # mb to fb
        print('Reported Sigma Error (Pythia) [fb]: ' + '{:.12e}'.format(meta['sigma_error'] * 1e12))  # mb to fb
        print('Processed Events (Pythia): ' + '{:f}'.format(meta['events_processed']))
        print('Processed Weight Sum (Pythia): ' + '{:f}'.format(meta['events_processed_weighted']))
        print('Processed Sigma (Pythia) [fb]: ' + '{:.12e}'.format(meta['processed_sigma'] * 1e12))  # mb to fb
        print('Processed Sigma Error (Pythia) [fb]: ' + '{:.12e}'.format(
            meta['processed_sigma_error'] * 1e12))  # mb to fb

        # PROCESS EVENTS
        for event in root_file.evt:
            # SHORT-CIRCUIT: MUST PASS LOW ENERGY CUTOFF
            if dataset_low_cutoff != -1 and event.rec_Q2 < dataset_low_cutoff:
                continue

            # SHORT-CIRCUIT: MUST PASS UP ENERGY CUTOFF
            if use_upper_energy_cut and -1 != dataset_up_cutoff and dataset_up_cutoff <= event.rec_Q2:
                continue

            # UPDATE TOTAL XSEC
            # Note: This is placed here in case of stitching.
            # It will sum all the event weights, for the requested Q^2 range.
            available_sigma += event.evt_weight * meta['sigma'] / meta['weight_sum'] * 1e12
            available_sigma_error += (event.evt_weight * meta['sigma_error'] / meta['weight_sum'] * 1e12) ** 2

            # SHORT-CIRCUIT: SAMPLE SIZE
            if -1 < event_limit <= processed_n:
                continue

            # SHORT-CIRCUIT: DETECTOR FIDUCIAL CUTS
            if not detector.passes_fiducial_cuts(event):
                continue

            # UPDATE PROCESSED XSEC
            processed_n += 1
            processed_sigma += event.evt_weight * meta['sigma'] / meta['weight_sum'] * 1e12
            processed_sigma_error += (event.evt_weight * meta['sigma_error'] / meta['weight_sum'] * 1e12) ** 2

            # PROCESS
            analyzer.process_entry((meta, event))

        # CLOSE
        root_file.Close()

        # INCREASE DATASET ID
        dataset_id += 1

    print('Summary')

    gen_sigma_error = math.sqrt(gen_sigma_error)
    available_sigma_error = math.sqrt(available_sigma_error)
    processed_sigma_error = math.sqrt(processed_sigma_error)

    print('Sigma (Generator) [fb]: ' + '{:.12e}'.format(gen_sigma))
    print('Sigma Error (Generator) [fb]: ' + '{:.12e}'.format(gen_sigma_error))
    print('Sigma (Available) [fb]: ' + '{:.12e}'.format(available_sigma))
    print('Sigma Error (Available) [fb]: ' + '{:.12e}'.format(available_sigma_error))
    print('Sigma (Processed) [fb]: ' + '{:.12e}'.format(processed_sigma))
    print('Sigma Error (Processed) [fb]: ' + '{:.12e}'.format(processed_sigma_error))

    # WRITE OUTPUTS
    gROOT.ProcessLine("""
        struct meta_t {
            Double_t gen_sigma;
            Double_t gen_sigma_error;
            Double_t available_sigma;
            Double_t available_sigma_error;
            Double_t processed_sigma;
            Double_t processed_sigma_error;
        };
    """)

    output_root_file = TFile.Open(os.path.join(root_file_output_dir, study + '.root'), 'RECREATE')

    meta = ROOT.meta_t()

    meta_tree = TTree('meta', 'Metadata')
    meta_tree.Branch('gen_sigma', addressof(meta, 'gen_sigma'), 'gen_sigma/D')
    meta_tree.Branch('gen_sigma_error', addressof(meta, 'gen_sigma_error'), 'gen_sigma_error/D')
    meta_tree.Branch('available_sigma', addressof(meta, 'available_sigma'), 'available_sigma/D')
    meta_tree.Branch('available_sigma_error', addressof(meta, 'available_sigma_error'), 'available_sigma_error/D')
    meta_tree.Branch('processed_sigma', addressof(meta, 'processed_sigma'), 'processed_sigma/D')
    meta_tree.Branch('processed_sigma_error', addressof(meta, 'processed_sigma_error'), 'processed_sigma_error/D')

    meta.gen_sigma = gen_sigma
    meta.gen_sigma_error = gen_sigma_error
    meta.available_sigma = available_sigma
    meta.available_sigma_error = available_sigma_error
    meta.processed_sigma = processed_sigma
    meta.processed_sigma_error = processed_sigma_error

    meta_tree.Fill()
    meta_tree.Write()

    analyzer.write()

    output_root_file.Close()


def analyze_sensitivity(study, request, parameters):
    # BEGIN SIGNAL ANALYSIS
    sensitivity_analyzer = SensitivityAnalyzer(parameters)

    for dataset in request['datasets']:
        # UNPACK DATASET
        name = dataset['name']
        root_file_path = os.path.join('..', dataset['input'])

        # LOG
        print('Reading: %s' % root_file_path)

        # OPEN
        root_file = TFile.Open(root_file_path, 'READ')

        # PROCESS
        sensitivity_analyzer.process_entry((name, root_file))

        # CLOSE
        root_file.Close()

    sensitivity_analyzer.write()
    # END SIGNAL ANALYSIS

    # BEGIN ZP PSCAN ANALYSIS
    zp_pscan_analyzer = ZPrimePScanAnalyzer(parameters)

    pscan_path = os.path.join('..', request['zp_pscan'])

    with open(pscan_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        skip_rows = 1
        skipped_rows = 0

        for row in reader:
            if skipped_rows < skip_rows:
                skipped_rows += 1
                continue

            zp_pscan_analyzer.process_entry(row)

    zp_pscan_analyzer.write()
    # END ZP PSCAN ANALYSIS

    # BEGIN INT PSCAN ANALYSIS
    intf_pscan_analyzer = IntfPScanAnalyzer(parameters)

    pscan_path = os.path.join('..', request['intf_pscan'])

    with open(pscan_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        skip_rows = 1
        skipped_rows = 0

        for row in reader:
            if skipped_rows < skip_rows:
                skipped_rows += 1
                continue

            intf_pscan_analyzer.process_entry(row)

    intf_pscan_analyzer.write()
    # END INT PSCAN ANALYSIS

    # BEGIN REACH ANALYSIS
    reach_min_cut_analyzer = ReachAnalyzer(parameters, 'min_cut')

    # LOOP DATASETS
    for var in sensitivity_analyzer.req_xsec_min_cut.keys():
        req_xsec = sensitivity_analyzer.req_xsec_min_cut[var]
        cur_xsec = sensitivity_analyzer.cur_xsec_min_cut[var]

        reach_min_cut_analyzer.process_entry({
            'mzp': request['mzp'],
            'var': var,
            'B1': zp_pscan_analyzer.fit_results[request['mzp']]['B'],
            'B1_err': zp_pscan_analyzer.fit_results[request['mzp']]['B_err'],
            'B2': intf_pscan_analyzer.fit_results[request['mzp']]['B'],
            'B2_err': intf_pscan_analyzer.fit_results[request['mzp']]['B_err'],
            'k': zp_pscan_analyzer.fit_results[request['mzp']]['k'],
            'k_err': zp_pscan_analyzer.fit_results[request['mzp']]['k_err'],
            'cut_info': sensitivity_analyzer.var_min_cut_info[var],
            'xsec-list': [
                {'label': '30 fb^{-1}', 'intlumi': 30, 'req_sigma': req_xsec[30], 'cur_sigma': cur_xsec[30]},
                {'label': '300 fb^{-1}', 'intlumi': 300, 'req_sigma': req_xsec[300], 'cur_sigma': cur_xsec[300]},
                {'label': '3000 fb^{-1}', 'intlumi': 3000, 'req_sigma': req_xsec[3000], 'cur_sigma': cur_xsec[3000]},
                # {'label': '237 fb^{-1} [10y]', 'intlumi': 237, 'req_sigma': req_xsec[237], 'cur_sigma': cur_xsec[237]},
                # {'label': '400 fb^{-1} [10y]', 'intlumi': 400, 'req_sigma': req_xsec[400], 'cur_sigma': cur_xsec[400]},
            ]
        })

    reach_min_cut_analyzer.write()
    # END REACH ANALYSIS


def analyze_comparison(study, request, parameters):
    # BEGIN SIGNAL ANALYSIS
    analyzer = ComparisonAnalyzer(parameters)

    for dataset in request['datasets']:
        # UNPACK DATASET
        name = dataset['name']
        root_file_path = os.path.join('..', dataset['input'])

        # LOG
        print('Reading: %s' % root_file_path)

        # OPEN
        root_file = TFile.Open(root_file_path, 'READ')

        # PROCESS
        analyzer.process_entry((name, root_file))

        # CLOSE
        root_file.Close()

    analyzer.write()
