import math

vev = 246.2196
vev_phi = 2500
trident_measured_over_sm = 1.58
trident_limit_factor = math.sqrt(
    2 / (math.sqrt(trident_measured_over_sm * (1 + (1 + 4 * 0.2223) ** 2) - 1) - 1 - 4 * 0.2223))
