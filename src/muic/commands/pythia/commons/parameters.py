import array
import math

import numpy as np

from muic.pythia.commons.colliders import machines


class ParameterSet(object):

    def __init__(self, machine_name):
        # COMPARISONS
        self.min_q2_dcs = 1e-18
        self.max_q2_dcs = 1e8

        # UNIFORM
        self.machine = machines[machine_name]
        self.com_energy_squared = 4 * self.machine['Emu'] * self.machine['Ep']
        self.min_q2 = 1
        self.max_q2 = self.com_energy_squared
        self.min_x = 1e-3
        self.max_x = 1
        self.min_y = 1e-2
        self.max_y = 1
        self.q2bins_uniform = np.arange(self.min_q2, 1e5, 10)
        self.xbins_uniform = np.linspace(1e-6, 1, 200)
        self.ybins_uniform = np.linspace(1e-2, 1, 100)
        self.eta_uniform = np.linspace(-10, 4, 70)
        self.angljb_uniform = np.linspace(0, 3.5, 100)
        self.sx_uniform = np.linspace(0, self.com_energy_squared, 200)

        # HIGH Q2 BINS
        self.q2bins_high = []

        for p in range(9):
            for i in range(1, 10):
                q2 = i * 10 ** p

                if q2 < 100:
                    continue

                self.q2bins_high.append(i * 10 ** p)

        self.q2bins_high = array.array('d', self.q2bins_high)

        # Q2 BINS
        self.q2bins = []

        for p in range(9):
            for i in range(1, 10):
                q2 = i * 10 ** p

                if q2 < self.min_q2:
                    continue

                self.q2bins.append(i * 10 ** p)

        self.q2bins = array.array('d', self.q2bins)

        # XBINS
        self.xbins = []

        for p in range(6):
            for i in range(1, 10):
                self.xbins.append(i * math.pow(10, p - 6))

        self.xbins.append(1)

        self.xbins = array.array('d', self.xbins)
