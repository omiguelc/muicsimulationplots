import json
import os

from ROOT import TFile

from muic.commands.pythia.kinematics.kinematics import KinematicsAnalyzer
from muic.pythia.analysis.pythia_engine import mt_run
from muic.pythia.producers.DISReconstructionProducer import DISReconstructionProducer


def configure_parser(parser):
    parser.add_argument('--nevents', dest='max_events', metavar='max_events',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--machine', dest='machine',
                        default='muic', type=str,
                        help='Specifies the machine being studied (MuIC, MuIC2, LHmuC)')

    parser.add_argument('name', metavar='NAME',
                        default='name', type=str,
                        help='Name of the Study')

    parser.add_argument('datasets', metavar='DATASETS',
                        default=[], type=str, nargs='+',
                        help='List of datasets')


def run(args):
    # Unpack args
    max_events = args.max_events
    name = args.name
    machine = args.machine
    datasets = [json.loads(dataset) for dataset in args.datasets]

    # Normalize paths
    ndatasets = []

    for dataset in datasets:
        ndatasets.append((
            os.path.join('../..', dataset['input']),
            dataset.get('sigma'), dataset.get('sigma_error'),
            dataset['min_q2'], dataset['max_q2']
        ))

    # Run
    os.makedirs('root_files', exist_ok=True)
    os.makedirs(name, exist_ok=True)
    os.chdir(name)

    run_analysis('../root_files', name, ndatasets, machine, max_events=max_events)

    os.chdir('../..')


def run_analysis(out_dir, name, datasets, machine, max_events=-1):
    # Build Analysis
    main_analyzers = [
        KinematicsAnalyzer(machine, 'true'),
        KinematicsAnalyzer(machine, 'lep'),
        KinematicsAnalyzer(machine, 'jb'),
        KinematicsAnalyzer(machine, 'da'),
    ]

    def analysis_fn():
        return [
            DISReconstructionProducer(machine),
        ], [
            KinematicsAnalyzer(machine, 'true'),
            KinematicsAnalyzer(machine, 'lep'),
            KinematicsAnalyzer(machine, 'jb'),
            KinematicsAnalyzer(machine, 'da'),
        ]

    def merge_fn(outputs):
        for worker in outputs:
            for analyzer_id in range(len(worker)):
                analyzer = worker[analyzer_id]
                main_analyzers[analyzer_id].merge(analyzer)

    # Run Analysis
    mt_run(datasets, max_events, os.cpu_count() // 2, analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open(os.path.join(out_dir, name + '.root'), 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
