# sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error

# MLQ,SIGMA_Q2min_1,SIGMA_Q2min_1_ERR,SIGMA_Q2min_20k,SIGMA_Q2min_20k_ERR
nc_bj="1685200e-12    232e-12       62960e-12     10e-12        1242.7e-12    0.234e-12"
nc_cj="9809000e-12    1830e-12      312500e-12    55.8e-12      4417.4e-12    1.45e-12"
nc_lj="21070000e-12   5720e-12      857420e-12    337e-12       30572e-12     11.9e-12"
lq200="1.0e-12        1.0e-12       1.0e-12       1.0e-12       1.0e-12       1.0e-12"
int200="-2.8e-10         2.139e-13          1.0e-12       1.0e-12       1.0e-12       1.0e-12"

# NC
function study_background() {
  ../bin/run standard-model --machine muic --individual muic_nc_bj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$1"', "sigma_error": '"$2"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run standard-model --machine muic --individual muic_nc_cj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run standard-model --machine muic --individual muic_nc_lj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${15}"', "sigma_error": '"${16}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${17}"', "sigma_error": '"${18}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
}

function study_signal() {
#  ../bin/run leptoquark --machine muic --individual "muic_lq_intf" \
#  '{
#    "datasets": [
#      {"input": "../in_lq/Int_100k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 0, "max_q2": -1}
#    ]
#  }' &

  # AGGREGATE
  ../bin/run leptoquark --machine muic --signal "muic_lq_signal" \
    '{
    "mzp": '"$1"',
    "datasets": [
      {"name": "interference", "input": "root_files/muic_lq_intf.root"},
      {"name": "nc-b-jet", "input": "root_files/muic_nc_bj_mg5.root"},
      {"name": "nc-c-jet", "input": "root_files/muic_nc_cj_mg5.root"},
      {"name": "nc-l-jet", "input": "root_files/muic_nc_lj_mg5.root"}
    ]
  }' &
}

#study_background $nc_bj $nc_cj $nc_lj
study_signal 500 '' $lq200 $int200
