# sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error

# MZP,SIGMA_Q2min_1,SIGMA_Q2min_1_ERR,SIGMA_Q2min_20k,SIGMA_Q2min_20k_ERR
nc_bj="10077000e-12 1680e-12    643420e-12    130e-12       36094e-12     7.54e-12"
nc_cj="51776000e-12 13300e-12   2802100e-12   610e-12       108510e-12    38.4e-12"
nc_lj="90675000e-12 26200e-12   5105200e-12   1600e-12      263120e-12    106e-12"
zp200="0.070103e-12 1.04e-17    0.26961e-12   9.22e-17      0.2964e-12    0.0001e-12"
zp350="0.071309e-12 1.03e-17    0.31051e-12   0.000104e-12  0.66489e-12   0.000228e-12"
zp500="0.071622e-12 1.06e-17    0.3227e-12    0.000113e-12  0.96647e-12   0.000354e-12"
int200="955.88e-12  0.236e-12   468.9e-12     0.121e-12     97.962e-12    0.0247e-12"
int350="961.61e-12  0.228e-12   492.83e-12    0.13e-12      133.61e-12    0.0338e-12"
int500="963.34e-12  0.274e-12   499.38e-12    0.133e-12     149.69e-12    0.0374e-12"

# NC
function study_background() {
  ../bin/run zprime --machine lhmuc --individual lhmuc_nc_bj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_bj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$1"', "sigma_error": '"$2"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_bj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_bj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine lhmuc --individual lhmuc_nc_cj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_cj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_cj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_cj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine lhmuc --individual lhmuc_nc_lj_mg5 \
  '{
    "datasets": [
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_lj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_lj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${15}"', "sigma_error": '"${16}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/LHmuC_mz_200GeV_delbs_0p1_nc_lj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${17}"', "sigma_error": '"${18}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
}

function study_signal() {
#  # SIGNAL
#  ../bin/run zprime --machine lhmuc --individual "lhmuc_mz_$1GeV_$2_zp_sb_to_b" \
#  '{
#    "datasets": [
#      {"input": "../in/zprime/LHmuC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 100, "max_q2": 1000},
#      {"input": "../in/zprime/LHmuC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_1k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 1000, "max_q2": 10000},
#      {"input": "../in/zprime/LHmuC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_10k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 10000, "max_q2": -1}
#    ]
#  }' &
#  ../bin/run zprime --machine lhmuc --individual "lhmuc_mz_$1GeV_$2_nc_bj_intf" \
#  '{
#    "datasets": [
#      {"input": "../in/intf/LHmuC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 100, "max_q2": 1000},
#      {"input": "../in/intf/LHmuC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 1000, "max_q2": 10000},
#      {"input": "../in/intf/LHmuC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 10000, "max_q2": -1}
#    ]
#  }' &

  # AGGREGATE
  ../bin/run zprime --machine lhmuc --signal "lhmuc_mz_$1GeV_$2_signal" \
  '{
    "mzp": '"$1"',
    "zp_pscan": "../in/xsec/zprime_lhmuc_pscan_q2min_100_xsec.csv",
    "intf_pscan": "../in/xsec/nc_bj_intf_lhmuc_pscan_q2min_100_xsec.csv",
    "datasets": [
      {"name": "zprime", "input": "root_files/lhmuc_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b.root"},
      {"name": "interference", "input": "root_files/lhmuc_mz_'"$1"'GeV_'"$2"'_nc_bj_intf.root"},
      {"name": "nc-b-jet", "input": "root_files/lhmuc_nc_bj_mg5.root"},
      {"name": "nc-c-jet", "input": "root_files/lhmuc_nc_cj_mg5.root"},
      {"name": "nc-l-jet", "input": "root_files/lhmuc_nc_lj_mg5.root"}
    ]
  }' &
}

#study_background $nc_bj $nc_cj $nc_lj
study_signal 200 'delbs_0p1' $zp200 $int200
study_signal 350 'delbs_0p1' $zp350 $int350
study_signal 500 'delbs_0p1' $zp500 $int500
