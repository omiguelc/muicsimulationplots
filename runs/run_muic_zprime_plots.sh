# sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error

# MZP,SIGMA_Q2min_1,SIGMA_Q2min_1_ERR,SIGMA_Q2min_20k,SIGMA_Q2min_20k_ERR
nc_bj="1685200e-12    232e-12       62960e-12     10e-12        1242.7e-12    0.234e-12"
nc_cj="9809000e-12    1830e-12      312500e-12    55.8e-12      4417.4e-12    1.45e-12"
nc_lj="21070000e-12   5720e-12      857420e-12    337e-12       30572e-12     11.9e-12"
zp200="0.010115e-12   1.11e-18      0.021138e-12  5.79e-18      0.0078023e-12 1.75e-18"
zp350="0.010278e-12   1.15e-18      0.023946e-12  6.24e-18      0.013711e-12  3.19e-18"
zp500="0.010321e-12   1.15e-18      0.024749e-12  6.72e-18      0.016705e-12  4.09e-18"
int200="148.89e-12    0.0286e-12    41.98e-12     0.00831e-12   3.096e-12     0.000577e-12"
int350="149.78e-12    0.0275e-12    43.826e-12    0.0086e-12    3.9343e-12    0.000684e-12"
int500="149.9e-12     0.0302e-12    44.324e-12    0.00895e-12   4.2404e-12    0.000727e-12"

# NC
function study_background() {
  ../bin/run zprime --machine muic --individual muic_nc_bj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$1"', "sigma_error": '"$2"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_bj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine muic --individual muic_nc_cj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_cj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine muic --individual muic_nc_lj_mg5 \
    '{
    "datasets": [
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${15}"', "sigma_error": '"${16}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/nc/MuIC_mz_200GeV_delbs_0p1_nc_lj_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${17}"', "sigma_error": '"${18}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
}

function study_signal() {
  # SIGNAL
  ../bin/run zprime --machine muic --individual "muic_mz_$1GeV_$2_zp_sb_to_b" \
  '{
    "datasets": [
      {"input": "../in/zprime/MuIC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$3"', "sigma_error": '"$4"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/zprime/MuIC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_1k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$5"', "sigma_error": '"$6"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/zprime/MuIC_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b_q2_10k_ymin_0p01_ymax_0p9_mg5_300k.root", "sigma": '"$7"', "sigma_error": '"$8"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &
  ../bin/run zprime --machine muic --individual "muic_mz_$1GeV_$2_nc_bj_intf" \
  '{
    "datasets": [
      {"input": "../in/intf/MuIC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_100_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"$9"', "sigma_error": '"${10}"', "min_q2": 100, "max_q2": 1000},
      {"input": "../in/intf/MuIC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_1k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${11}"', "sigma_error": '"${12}"', "min_q2": 1000, "max_q2": 10000},
      {"input": "../in/intf/MuIC_mz_'"$1"'GeV_'"$2"'_nc_bj_intf_q2_10k_ymin_0p01_ymax_0p9_mg5_600k.root", "sigma": '"${13}"', "sigma_error": '"${14}"', "min_q2": 10000, "max_q2": -1}
    ]
  }' &

#  # AGGREGATE
#  ../bin/run zprime --machine muic --signal "muic_mz_$1GeV_$2_signal" \
#    '{
#    "mzp": '"$1"',
#    "zp_pscan": "../in/xsec/zprime_muic_pscan_q2min_100_xsec.csv",
#    "intf_pscan": "../in/xsec/nc_bj_intf_muic_pscan_q2min_100_xsec.csv",
#    "datasets": [
#      {"name": "zprime", "input": "root_files/muic_mz_'"$1"'GeV_'"$2"'_zp_sb_to_b.root"},
#      {"name": "interference", "input": "root_files/muic_mz_'"$1"'GeV_'"$2"'_nc_bj_intf.root"},
#      {"name": "nc-b-jet", "input": "root_files/muic_nc_bj_mg5.root"},
#      {"name": "nc-c-jet", "input": "root_files/muic_nc_cj_mg5.root"},
#      {"name": "nc-l-jet", "input": "root_files/muic_nc_lj_mg5.root"}
#    ]
#  }' &
}

study_background $nc_bj $nc_cj $nc_lj
#study_signal 200 'delbs_0p1' $zp200 $int200
#study_signal 350 'delbs_0p1' $zp350 $int350
#study_signal 500 'delbs_0p1' $zp500 $int500
