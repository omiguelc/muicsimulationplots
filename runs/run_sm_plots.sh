wd=$(pwd)

../bin/run pythia-kinematics test_fw \
--nevents -1 --machine 'muic' \
'{"name": "1", "input": "in_sm2/muic_nc_mu_q2_1_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 1, "max_q2": 10}' \
'{"name": "10", "input": "in_sm2/muic_nc_mu_q2_10_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 10, "max_q2": 100}' \
'{"name": "100", "input": "in_sm2/muic_nc_mu_q2_100_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 100, "max_q2": 1000}' \
'{"name": "1000", "input": "in_sm2/muic_nc_mu_q2_1k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 1000, "max_q2": 10000}' \
'{"name": "10000", "input": "in_sm2/muic_nc_mu_q2_10k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 10000, "max_q2": 100000}' \
'{"name": "100000", "input": "in_sm2/muic_nc_mu_q2_100k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 100000, "max_q2": -1}' \

#../bin/run pythia-kinematics muic_nc_mu_ymin_0p01_ymax_0p9_pythia \
#--nevents 1000 --machine 'muic' \
#'{"name": "1", "input": "in_sm/muic_nc_mu_q2_1_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 1, "max_q2": 10}' \
#'{"name": "10", "input": "in_sm/muic_nc_mu_q2_10_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 10, "max_q2": 100}' \
#'{"name": "100", "input": "in_sm/muic_nc_mu_q2_100_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 100, "max_q2": 1000}' \
#'{"name": "1000", "input": "in_sm/muic_nc_mu_q2_1k_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 1000, "max_q2": 10000}' \
#'{"name": "10000", "input": "in_sm/muic_nc_mu_q2_10k_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 10000, "max_q2": 100000}' \
#'{"name": "100000", "input": "in_sm/muic_nc_mu_q2_100k_ymin_0p01_ymax_0p9_pythia_150k.root", "sigma": null, "sigma_error": null, "min_q2": 100000, "max_q2": -1}' \

#task_name='ml_samples_v2'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#../../bin/run pythia-ml-sample --filename muic_nc_mu_ymin_0p01_ymax_0p9 \
#--nevents -1 --machine 'muic' \
#'{"name": "1", "input": "../../in_sm2/muic_nc_mu_q2_1_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 1, "max_q2": 10}' \
#'{"name": "10", "input": "../../in_sm2/muic_nc_mu_q2_10_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 10, "max_q2": 100}' \
#'{"name": "100", "input": "../../in_sm2/muic_nc_mu_q2_100_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 100, "max_q2": 1000}' \
#'{"name": "1000", "input": "../../in_sm2/muic_nc_mu_q2_1k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 1000, "max_q2": 10000}' \
#'{"name": "10000", "input": "../../in_sm2/muic_nc_mu_q2_10k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 10000, "max_q2": 100000}' \
#'{"name": "100000", "input": "../../in_sm2/muic_nc_mu_q2_100k_ymin_0p01_ymax_0p9_pythia_1k.root", "sigma": null, "sigma_error": null, "min_q2": 100000, "max_q2": -1}'
#cd "$wd"